// @file volcon.ino
// @brief USB-Volume controller.
// @author ishida.haruyuki@fujitsu.com
// @note Thir sketch references two libraries, that are MsTimer2 and HID-Project.
//       MsTimer2: http://www.pjrc.com/teensy/td_libs_MsTimer2.html
//       HID-Project: https://github.com/NicoHood/HID

#include "RotaryEncoder.h"
#include "PullUppedButton.h"
#include <MsTimer2.h>
#include <HID-Project.h>

/*
  Parts:
     Micro Computer bord: https://www.sparkfun.com/products/12587
     RotaryEncoder: https://www.sparkfun.com/products/15140
     RotaryEncoder pitch conversion PCB: https://www.sparkfun.com/products/11722
*/

const uint8_t PIN_MUTE_BUTTON = 9;
const uint8_t PIN_ROTARY_ENCODER_A = 15;
const uint8_t PIN_ROTARY_ENCODER_B = 14;

const uint8_t COUNT_DEBOUNCE_MUTE_BUTTON = 3;     // Button Debouncing Time: 3 milli second.
const uint8_t COUNT_DEBOUNCE_ROTARY_ENCODER = 3;  // Rotary Encoder Debouncing Time: 3 milli second.

PullUppedButtonClass muteButton(PIN_MUTE_BUTTON, COUNT_DEBOUNCE_MUTE_BUTTON);
RotaryEncoderClass rotaryEncoder(PIN_ROTARY_ENCODER_A, PIN_ROTARY_ENCODER_B, COUNT_DEBOUNCE_ROTARY_ENCODER);

bool muteButtonPressed = false;
int8_t rotation = 0;


void onTimer() {
  auto rm = muteButton.update();
  auto rr = rotaryEncoder.update();
  if (rm) {
    muteButtonPressed = muteButtonPressed || muteButton.isPressed();
  }

  if (muteButtonPressed){
    rotation = 0;
  } else {
    rotation += rr;
  }
}


void sendConsumerKey(ConsumerKeycode key, int count) {
  for (int i = 0; i < count; ++i) {
    Consumer.write(key);
  }
}


void setup() {
  muteButton.begin();
  rotaryEncoder.begin();
  Consumer.begin();
  MsTimer2::set(1, onTimer);
  MsTimer2::start();
}


void loop() {
  cli();
  bool muteButtonPressedTemp = muteButtonPressed;
  muteButtonPressed = false;
  int32_t rotationTemp = rotation;
  rotation = 0;
  sei();

  if (muteButtonPressedTemp) {
    Consumer.write(MEDIA_VOLUME_MUTE);
  }

  if (rotationTemp > 0) {
    sendConsumerKey(MEDIA_VOLUME_UP, rotationTemp);
  } else if (rotationTemp < 0) {
    sendConsumerKey(MEDIA_VOLUME_DOWN, -rotationTemp);
  }
}
