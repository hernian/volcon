// @file RotaryEncoder.cpp
// @brief Reads RotaryEncoder value, debounce it, and, handle graycode.


#include "RotaryEncoder.h"


static const int8_t RotaryEncoderClass::_decodeGrayCodeAB[] PROGMEM = {
  /* prev cur  */
  /* A B  A B */
  /* 0 0  0 0 */  0,      /* stay */
  /* 0 0  0 1 */ -1,
  /* 0 0  1 0 */  1,
  /* 0 0  1 1 */  0,      /* invalid state changing */
  /* 0 1  0 0 */  1,
  /* 0 1  0 1 */  0,      /* stay */
  /* 0 1  1 0 */  0,      /* invalid state changing */
  /* 0 1  1 1 */ -1,
  /* 1 0  0 0 */ -1,
  /* 1 0  0 1 */  0,      /* invalid state changing */
  /* 1 0  1 0 */  0,      /* stay */
  /* 1 0  1 1 */  1,
  /* 1 1  0 0 */  0,      /* invalid state changing */
  /* 1 1  0 1 */  1,
  /* 1 1  1 0 */ -1,
  /* 1 1  1 1 */  0       /* stay */
};



