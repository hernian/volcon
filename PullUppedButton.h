// @file Button.h
// @brief Button for PULL-UP, with debouncing.

#pragma once

#include <Arduino.h>
#include "Debouncer.h"

class PullUppedButtonClass {
  public:
    PullUppedButtonClass(uint8_t pin, uint8_t countDebounce)
      : _pin(pin), _value(HIGH), _debouncer(countDebounce) {
      // nothing to do.
    }
    
    void begin() {
      pinMode(_pin, INPUT_PULLUP);
      auto value = HIGH;
      _debouncer.begin(value);
      _value = value;      
    }
    
    bool update() {
      auto value = digitalRead(_pin);
      auto debounced = _debouncer.update(value);
      if (debounced){
        _value = value;
      }
      return debounced;
    }

    bool isPressed() const {
      return (_value == LOW);
    }

  private:
    uint8_t _pin;
    int8_t  _value;
    DebouncerClass _debouncer;
};


