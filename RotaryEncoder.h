// @file RotaryEncoder.h
// @brief decode rorary encoder signal.

#pragma once

#include <Arduino.h>
#include "Debouncer.h"


class RotaryEncoderClass {
  public:
    RotaryEncoderClass(uint8_t pinA, uint8_t pinB, uint8_t countDebounce)
      : _pinA(pinA), _pinB(pinB), _debouncerAB(countDebounce) {
        // nothing to do.
    }
 
    void begin() {
      pinMode(_pinA, INPUT_PULLUP);
      pinMode(_pinB, INPUT_PULLUP);
      _valueAB = readAB();
      _debouncerAB.begin(_valueAB);
    }
    
    int8_t update() {
      auto valueAB = readAB();
      auto debounced = _debouncerAB.update(valueAB);
      if (debounced == false){
        return 0;
      }
      uint8_t valueABPrev = _valueAB;
      _valueAB = valueAB;
      uint8_t index = (valueABPrev << 2) | valueAB;
      auto res = pgm_read_byte(_decodeGrayCodeAB + index);
      return res;
    }

  private:
    uint8_t readAB() {
      auto valueA = (digitalRead(_pinA) == LOW)? 0x00: 0x02;
      auto valueB = (digitalRead(_pinB) == LOW)? 0x00: 0x01;
      uint8_t valueAB = valueA | valueB;
      return valueAB;  
    }

  private:
    static const int8_t _decodeGrayCodeAB[] PROGMEM;
 
  private:
    uint8_t _pinA;
    uint8_t _pinB;
    uint8_t _valueAB;
    DebouncerClass _debouncerAB;
};




