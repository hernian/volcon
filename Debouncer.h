// @file DebouncedDigitalReader.h
// @brief Read a pin value, and debouce it.
// @author herneanrunner@gmail.com

#pragma once

#include <Arduino.h>


class DebouncerClass {
  public:
    DebouncerClass(uint8_t countDebounce)
        : _countDebounce(countDebounce), _count(0), _valueCur(0) {
    }

    void  begin(uint8_t value) {
        _count = 0;
        _valueCur = value;
    }
    
    bool  update(uint8_t value) {
      bool r = false;
      if (_count == 0) {
        if (value != _valueCur){
          _count = _countDebounce;
        }
      } else {
        --_count;
        if (_count == 0) {
          _valueCur = value;
          r = true;
        }
      }
      return r;
    }

    int getValue(){
        return _valueCur;
    }

  private:
    const uint8_t _pin;
    const uint8_t _countDebounce;
    uint8_t _count;
    int     _valueCur;
};


